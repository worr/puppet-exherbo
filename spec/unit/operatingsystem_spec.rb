#!/usr/bin/env ruby
#
# Copyright (c) 2014 William Orr <will@worrbase.com>
# 
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
# 
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
# 
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
# THE SOFTWARE.

require 'facter'
require 'facter/exherbo'
require 'spec_helper'

describe '$operatingsystem' do
  it 'should be Exherbo if /etc/os-release contains NAME=Exherbo' do
    exh = File.read('spec/fixtures/exherbo')
    FileTest.expects(:exists?).with('/etc/os-release').returns true
    File.expects(:read).returns exh

    expect(Facter.fact(:operatingsystem).value).to eq "Exherbo"
  end

  it 'should not be Exherbo if /etc/os-release does not exist' do
    FileTest.expects(:exists?).with('/etc/os-release').returns false

    expect(Facter.fact(:operatingsystem).value).not_to eq "Exherbo"
  end

  it 'should not be Exherbo if /etc/os-release does not contain NAME=Exherbo' do
    ne = File.read('spec/fixtures/notexherbo')
    FileTest.expects(:exists?).with('/etc/os-release').returns true
    File.expects(:read).returns ne

    expect(Facter.fact(:operatingsystem).value).not_to eq "Exherbo"
  end
end
